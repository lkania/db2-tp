analyze table SALE compute statistics;
analyze table REVIEW compute statistics;
analyze table EUSER compute statistics;
analyze table SCORE compute statistics;

set linesize 200;

select TABLE_NAME, NUM_ROWS, BLOCKS, EMPTY_BLOCKS, AVG_ROW_LEN, PCT_FREE, PCT_USED
from USER_TABLES 
where TABLE_NAME='SALE' 
/
select TABLE_NAME, NUM_ROWS, BLOCKS, EMPTY_BLOCKS, AVG_ROW_LEN, PCT_FREE, PCT_USED
from USER_TABLES 
where TABLE_NAME='REVIEW' 
/
select TABLE_NAME, NUM_ROWS, BLOCKS, EMPTY_BLOCKS, AVG_ROW_LEN, PCT_FREE, PCT_USED
from USER_TABLES 
where TABLE_NAME='EUSER' 
/
select TABLE_NAME, NUM_ROWS, BLOCKS, EMPTY_BLOCKS, AVG_ROW_LEN, PCT_FREE, PCT_USED
from USER_TABLES 
where TABLE_NAME='SCORE' 
/

show parameter block_size;

select INDEX_NAME,INDEX_TYPE,UNIQUENESS,COMPRESSION,TABLE_NAME,TABLE_TYPE from USER_INDEXES;
select INDEX_NAME,AVG_DATA_BLOCKS_PER_KEY,CLUSTERING_FACTOR from USER_INDEXES;


select table_name,i.index_name,substr(column_name,1,10) as COLUMN_NAME from user_tab_columns t natural join user_ind_columns i where table_name='REVIEW';


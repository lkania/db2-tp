CREATE OR REPLACE PACKAGE scoring_pkg_alt AS -- spec
  PROCEDURE calculate_seller_scoring_alt(ndays NUMBER);
END scoring_pkg_alt;

/

CREATE OR REPLACE PACKAGE BODY scoring_pkg_alt AS -- body

   -- Cursor de vendedores cuyas calificaciones sufrieron modificaciones
   -- o cuya reputacion hay que actualizar porque es "vieja" (de mas de 1 semana)
  CURSOR SELLER_CUR(pInterval NUMBER) IS
    SELECT DISTINCT seller_id AS user_id
    FROM sale s
    WHERE EXISTS (
      SELECT 1
     FROM REVIEW R
     WHERE s.id = r.sale_id
     AND MODIFIED >= (SYSDATE - pInterval)
    ) UNION (
      SELECT user_id
      FROM score
      WHERE modified >= (SYSDATE - 7)
    );

  -- Procedimiento que actualiza la reputacion de un vendedor
  PROCEDURE calculate_user_scores_alt (
    muser_id IN NUMBER,
    v7Positive OUT NUMBER,
    v7Negative OUT NUMBER,
    v7Neutral OUT NUMBER,
    v30Positive OUT NUMBER,
    v30Negative OUT NUMBER,
    v30Neutral OUT NUMBER,
    v180Positive OUT NUMBER,
    v180Negative OUT NUMBER,
    v180Neutral OUT NUMBER,
    vScoreTotal OUT NUMBER,
    vCountTotal OUT NUMBER
  ) IS

    CURSOR qualifications IS
       SELECT score, time_category, count(*) AS qty
       FROM (
        SELECT
          score,
          CASE
          WHEN created > SYSDATE - 7 THEN 7
          WHEN created > SYSDATE - 30 THEN 30
          WHEN created > SYSDATE - 180 THEN 180
          ELSE 0
          end AS time_category
       FROM review
       WHERE role = 'SELLER'
         AND user_id = muser_id
         AND status = 'PUBLISHED' )
       GROUP BY score, time_category;

    q qualifications%ROWTYPE;

    BEGIN
      v7Positive := 0;
      v7Negative := 0;
      v7Neutral := 0;
      v30Positive := 0;
      v30Negative := 0;
      v30Neutral := 0;
      v180Positive := 0;
      v180Negative := 0;
      v180Neutral := 0;
      vScoreTotal := 0;
      vCountTotal := 0;

      OPEN qualifications;

      LOOP

        FETCH qualifications INTO q;
        EXIT WHEN qualifications%NOTFOUND;

        vCountTotal := vCountTotal + q.qty;

        CASE
        WHEN q.score = 'POSITIVE' THEN
            vScoreTotal := vScoreTotal + q.qty;
            IF q.time_category >= 7 THEN
              v7Positive := v7Positive + q.qty;
              v30Positive := v30Positive + q.qty;
              v180Positive := v180Positive + q.qty;
            ELSIF q.time_category >= 30 THEN
              v30Positive := v30Positive + q.qty;
              v180Positive := v180Positive + q.qty;
            ELSIF q.time_category >= 180 THEN
              v180Positive := v180Positive + q.qty;
            END IF;
        WHEN q.score = 'NEGATIVE' THEN
            vScoreTotal := vScoreTotal - q.qty;
            IF q.time_category >= 7 THEN
              v7Negative := v7Negative + q.qty;
              v30Negative := v30Negative + q.qty;
              v180Negative := v180Negative + q.qty;
            ELSIF q.time_category >= 30 THEN
              v30Negative := v30Negative + q.qty;
              v180Negative := v180Negative + q.qty;
            ELSIF q.time_category >= 180 THEN
              v180Negative := v180Negative + q.qty;
            END IF;
        WHEN q.score = 'NEUTRAL' THEN
            IF q.time_category >= 7 THEN
              v7Neutral := v7Neutral + q.qty;
              v30Neutral := v30Neutral + q.qty;
              v180Neutral := v180Neutral + q.qty;
            ELSIF q.time_category >= 30 THEN
              v30Neutral := v30Neutral + q.qty;
              v180Neutral := v180Neutral + q.qty;
            ELSIF q.time_category >= 180 THEN
              v180Neutral := v180Neutral + q.qty;
            END IF;
        END CASE;
      END LOOP;
    END;

    PROCEDURE update_seller_scoring_alt(
      pUserId NUMBER, p7Positive NUMBER, p7Negative NUMBER,
      p7Neutral NUMBER, p30Positive NUMBER, p30Negative NUMBER,
      p30Neutral NUMBER, p180Positive NUMBER, p180Negative NUMBER,
      p180Neutral NUMBER, pScoreTotal NUMBER, pCountTotal NUMBER
    ) AS vRATIO NUMBER(10, 2) := 0;

    BEGIN
      IF pCountTotal <= 0
        THEN vRATIO := 0;
        ELSE vRATIO := round(pScoreTotal / pCountTotal, 2);
      END IF; --Intento la actualizacion

      UPDATE SCORE
      SET last_week_positive = p7Positive,
          last_week_neutral = p7Neutral,
          last_week_negative = p7Negative,
          last_month_positive = p30Positive,
          last_month_neutral = p30Neutral,
          last_month_negative = p30Negative,
          last_6month_positive = p180Positive,
          last_6month_neutral = p180Neutral,
          last_6month_negative = p180Negative,
          score = vRATIO,
          modified = sysdate
      WHERE USER_ID = pUserId; -- Si no existe, inserto el registro

      IF SQL%NOTFOUND THEN
        INSERT INTO score
          (user_id, last_week_positive, last_week_neutral, last_week_negative, last_month_positive, last_month_neutral, last_month_negative, last_6month_positive, last_6month_neutral, last_6month_negative, score, status, created, modified)
        VALUES
          (pUserId, p7Positive, p7Neutral, p7Negative, p30Positive, p30Neutral, p30Negative, p180Positive, p180Neutral, p180Negative, vRATIO, 'ACTIVE', SYSDATE, SYSDATE);
      END IF;

    COMMIT;

  END update_seller_scoring_alt;

  PROCEDURE calculate_seller_scoring_alt(ndays NUMBER) AS

    v7Positive   NUMBER(10);
    v7Negative   NUMBER(10);
    v7Neutral    NUMBER(10);
    v30Positive  NUMBER(10);
    v30Negative  NUMBER(10);
    v30Neutral   NUMBER(10);
    v180Positive NUMBER(10);
    v180Neutral  NUMBER(10);
    v180Negative NUMBER(10);
    vCountTotal  NUMBER(10);
    vScoreTotal  NUMBER(10);

  BEGIN

    -- Por cada seller,
    FOR seller_rec IN SELLER_CUR(ndays) LOOP

      calculate_user_scores_alt(seller_rec.user_id,
          v7Positive, v7Negative, v7Neutral,
          v30Positive, v30Negative, v30Neutral,
          v180Positive, v180Negative, v180Neutral,
          vScoreTotal, vCountTotal
      );

      -- Insertar el puntaje actualizado
      update_seller_scoring_alt(seller_rec.user_id,
          v7Positive, v7Negative, v7Neutral,
          v30Positive, v30Negative, v30Neutral,
          v180Positive, v180Negative, v180Neutral,
          vScoreTotal, vCountTotal
      );

    END LOOP;

  END calculate_seller_scoring_alt;

END scoring_pkg_alt;

/
